FactoryBot.define do
  factory :billing do
    desired_due_day { 1 }
    student { create(:student) }
    course_value { 500 }
    status { "openned" }
  end
end
