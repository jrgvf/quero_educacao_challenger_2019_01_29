FactoryBot.define do
  factory :student do
    name { "MyString" }
    cpf { CPF.new(CPF.generate).formatted }
  end
end
