FactoryBot.define do
  factory :payment do
    value { 500 }
    status { "payed" }
    expiry_date { Time.zone.now.to_datetime }
  end
end
