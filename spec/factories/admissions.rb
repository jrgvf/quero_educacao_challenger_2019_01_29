FactoryBot.define do
  factory :admission do
    student { create(:student) }
    enem_grade { 451 }
    step { "admitted" }
  end
end
