require 'rails_helper'

RSpec.describe "Payments", type: :request do
  describe "post /billings/:cpf/payments" do
    context "fails" do
      let(:student_not_found_response) { "{\"message\":\"Student could not be found\"}" }
      let(:billing_not_found_response) { "{\"message\":\"Billing could not be found\"}" }
      let(:bills_not_found_response) { "{\"message\":\"Bill could not be found\"}" }
      let(:payment_already_performed_response) { "{\"message\":\"Payment already performed\"}" }
      let(:no_open_invoice_response) { "{\"message\":\"There is no open invoice to add the difference\"}" }
      let(:invalid_payment_response) { "{\"message\":\"Validation failed: Payments is invalid\"}" }

      it "when don't exists student" do
        post(api_v1_payments_path("12345678900"))
        expect(response).to have_http_status(404)
        expect(response.body).to eq(student_not_found_response)
      end

      it "when don't exists billing" do
        student = create(:student, cpf: "865.292.211-07")
        post(api_v1_payments_path("86529221107"))
        expect(response).to have_http_status(404)
        expect(response.body).to eq(billing_not_found_response)
      end

      it "when don't exists bill" do
        student = create(:student, cpf: "865.292.211-07")
        create(:billing, student: student)
        post(api_v1_payments_path("86529221107"), params: {payment: {year: 2019, month: 01}})
        expect(response).to have_http_status(404)
        expect(response.body).to eq(bills_not_found_response)
      end

      it "when payment already performed" do
        student = create(:student, cpf: "865.292.211-07")
        billing = build(:billing, student: student)
        billing.initialize_bills
        bill = billing.bills.first
        billing.save!
        create(:payment, method: "bank slip", bill: bill)

        post(api_v1_payments_path("86529221107"), params: {payment: {year: bill.year, month: bill.month}})
        expect(response).to have_http_status(422)
        expect(response.body).to eq(payment_already_performed_response)
      end

      it "when no open invoice to add the difference" do
        student = create(:student, cpf: "865.292.211-07")
        billing = build(:billing, student: student)
        billing.initialize_bills
        billing.save!
        bill = billing.bills.last

        post(api_v1_payments_path("86529221107"), params: {payment: {year: bill.year, month: bill.month, value: 400}})
        expect(response).to have_http_status(422)
        expect(response.body).to eq(no_open_invoice_response)
      end

      it "when payment value is less than or equal to 0" do
        student = create(:student, cpf: "865.292.211-07")
        billing = build(:billing, student: student)
        billing.initialize_bills
        billing.save!
        bill = billing.bills.first

        post(api_v1_payments_path("86529221107"), params: {payment: {year: bill.year, month: bill.month, value: 0}})
        expect(response).to have_http_status(422)
        expect(response.body).to eq(invalid_payment_response)
      end
    end

    context "success" do
      before { Timecop.freeze(Time.zone.parse("2019-01-01")) }
      after { Timecop.return }

      let(:payment_response) { File.read("spec/fixtures/payment_success_response.json") }
      let(:partial_payment_response) { File.read("spec/fixtures/partial_payment_success_response.json") }

      it "returns payment" do
        student = create(:student, cpf: "865.292.211-07")
        billing = build(:billing, student: student)
        billing.initialize_bills
        billing.save!
        bill = billing.bills.first

        post(api_v1_payments_path("86529221107"), params: {payment: {year: bill.year, month: bill.month, value: 500}})
        expect(response).to have_http_status(201)
        expect(response.body).to eq(payment_response)
      end

      it "returns payment and move difference to next bill when receive partial payment" do
        student = create(:student, cpf: "865.292.211-07")
        billing = build(:billing, student: student)
        billing.initialize_bills
        billing.save!
        bill = billing.bills.first

        post(api_v1_payments_path("86529221107"), params: {payment: {year: bill.year, month: bill.month, value: 400}})
        expect(response).to have_http_status(201)
        expect(response.body).to eq(partial_payment_response)

        next_bill = bill.next_bill
        expect(next_bill).not_to be_nil
        expect(next_bill.value).to eq(600)
      end
    end
  end
end
