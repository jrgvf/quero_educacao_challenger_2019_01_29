require 'rails_helper'

RSpec.describe "Bills", type: :request do
  describe "GET /billings/:cpf/bills" do
    context "fails" do
      let(:student_not_found_response) { "{\"message\":\"Student could not be found\"}" }
      let(:bills_not_found_response) { "{\"message\":\"Bills could not be found\"}" }

      it "when don't exists student" do
        get(api_v1_bills_path("12345678900"))
        expect(response).to have_http_status(404)
        expect(response.body).to eq(student_not_found_response)
      end

      it "when don't exists bills" do
        student = create(:student, cpf: "865.292.211-07")
        get(api_v1_bills_path("86529221107"))
        expect(response).to have_http_status(404)
        expect(response.body).to eq(bills_not_found_response)
      end
    end

    context "success" do
      before { Timecop.freeze(Time.zone.parse("2019-01-01")) }
      after { Timecop.return }

      let(:success_response) { File.read("spec/fixtures/bills_success_response.json") }

      it "returns empty when use status filter without entries" do
        student = create(:student, cpf: "865.292.211-07")
        create(:admission, student: student)
        billing = build(:billing, student: student, desired_due_day: 10)
        billing.initialize_bills
        billing.save!

        get(api_v1_bills_path("86529221107"), params: {status: "payed"})
        expect(response).to have_http_status(200)
        expect(response.body).to eq("[]")
      end

      it "returns bills" do
        student = create(:student, cpf: "865.292.211-07")
        create(:admission, student: student)
        billing = build(:billing, student: student, desired_due_day: 10)
        billing.initialize_bills
        billing.save!

        get(api_v1_bills_path("86529221107"), params: {status: "pending"})
        expect(response).to have_http_status(200)
        expect(response.body).to eq(success_response)
      end
    end
  end
end
