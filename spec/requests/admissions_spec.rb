require 'rails_helper'

RSpec.describe "Admissions", type: :request do
  describe "POST /admissions" do
    context "fails" do
      let(:without_admissions_key_response) { "{\"message\":\"param is missing or the value is empty: admission\"}" }
      let(:invalid_user_params_response) { "{\"message\":\"Validation failed: Name can't be blank, Cpf is invalid\"}" }
      let(:without_enem_grade_response) { "{\"message\":\"Validation failed: Enem grade is not a number\"}" }
      let(:lower_enem_grade_response) { "{\"message\":\"Validation failed: Enem grade must be greater than 450\"}" }
      let(:admission_already_exists_response) { "{\"message\":\"Admission already exists\"}" }

      it "when body has no admissions key" do
        post(api_v1_admissions_path, params: {})
        expect(response).to have_http_status(400)
        expect(response.body).to eq(without_admissions_key_response)
      end

      it "when body has invalid user params" do
        post(api_v1_admissions_path, params: {admission: {enem_grade: 451}})
        expect(response).to have_http_status(422)
        expect(response.body).to eq(invalid_user_params_response)
      end

      it "when body has no enem_grade" do
        post(api_v1_admissions_path, params: {admission: {name: "Jorge Rodrigues", cpf: CPF.generate}})
        expect(response).to have_http_status(422)
        expect(response.body).to eq(without_enem_grade_response)
      end

      it "when body has lower enem_grade" do
        post(api_v1_admissions_path, params: {admission: {name: "Jorge Rodrigues", cpf: CPF.generate, enem_grade: 450}})
        expect(response).to have_http_status(422)
        expect(response.body).to eq(lower_enem_grade_response)
      end

      it "when admission already exists" do
        student = create(:student, name: "Jorge Rodrigues")
        create(:admission, student: student)

        post(api_v1_admissions_path, params: {admission: {name: "Jorge Rodrigues", cpf: student.cpf, enem_grade: 452}})
        expect(response).to have_http_status(422)
        expect(response.body).to eq(admission_already_exists_response)
      end
    end

    context "success" do
      let(:success_response) { "{\"message\":\"Admission approved, please take your enrollment\"}" }

      it "create a student and create a admission" do
        student = Student.find_by(name: "Jorge Rodrigues")
        expect(student).to be_nil

        post(api_v1_admissions_path, params: {admission: {name: "Jorge Rodrigues", cpf: CPF.generate, enem_grade: 451}})
        expect(response).to have_http_status(201)
        expect(response.body).to eq(success_response)

        student = Student.find_by(name: "Jorge Rodrigues")
        expect(student).not_to be_nil
        admission = Admission.find_by(student: student)
        expect(admission).not_to be_nil
        expect(admission.enem_grade).to eq(451)
        expect(admission.step).to eq("admitted")
      end
    end
  end

  describe "GET /admissions/:cpf" do
    let(:admission_not_found_response) { "{\"message\":\"Admission could not be found\"}" }

    context "fails" do
      it "when don't exists admission for cpf" do
        get(api_v1_admission_path(CPF.generate))
        expect(response).to have_http_status(404)
        expect(response.body).to eq(admission_not_found_response)
      end
    end

    context "success" do
      it "when find admission for cpf" do
        student = create(:student, name: "Jorge Rodrigues", cpf: "800.512.745-66")
        admission = create(:admission, student: student)

        expected_response = {
          name: student.name,
          cpf: student.cpf,
          enem_grade: admission.enem_grade,
          step: admission.step,
          created_at: admission.created_at,
          updated_at: admission.updated_at
        }.to_json

        get(api_v1_admission_path("80051274566"))
        expect(response).to have_http_status(200)
        expect(response.body).to eq(expected_response)
      end
    end
  end
end
