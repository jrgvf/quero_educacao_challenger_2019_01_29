require 'rails_helper'

RSpec.describe "Enrollments", type: :request do
  describe "POST /enrollments" do
    context "fails" do
      let(:without_enrollment_key_response) { "{\"message\":\"param is missing or the value is empty: enrollment\"}" }
      let(:student_not_found_response) { "{\"message\":\"Student could not be found\"}" }
      let(:admission_not_found_response) { "{\"message\":\"Admission could not be found\"}" }
      let(:billing_already_exists_response) { "{\"message\":\"Enrollment already completed\"}" }
      let(:without_desired_due_day_response) { "{\"message\":\"Validation failed: Desired due day can't be blank\"}" }

      let(:lower_enem_grade_response) { "{\"message\":\"Validation failed: Enem grade must be greater than 450\"}" }
      let(:admission_already_exists_response) { "{\"message\":\"Admission already exists\"}" }

      it "when body has no enrollment key" do
        post(api_v1_enrollments_path, params: {})
        expect(response).to have_http_status(400)
        expect(response.body).to eq(without_enrollment_key_response)
      end

      it "when don't exists student" do
        post(api_v1_enrollments_path, params: {enrollment: {cpf: 12345678900}})
        expect(response).to have_http_status(404)
        expect(response.body).to eq(student_not_found_response)
      end

      it "when don't exists admission" do
        student = create(:student)
        post(api_v1_enrollments_path, params: {enrollment: {cpf: student.cpf}})
        expect(response).to have_http_status(404)
        expect(response.body).to eq(admission_not_found_response)
      end

      it "when billing already exists" do
        student = create(:student)
        create(:admission, student: student)
        create(:billing, student: student)

        post(api_v1_enrollments_path, params: {enrollment: {cpf: student.cpf}})
        expect(response).to have_http_status(422)
        expect(response.body).to eq(billing_already_exists_response)
      end

      it "when body has no desired_due_day" do
        student = create(:student)
        create(:admission, student: student)

        post(api_v1_enrollments_path, params: {enrollment: {cpf: student.cpf}})
        expect(response).to have_http_status(422)
        expect(response.body).to eq(without_desired_due_day_response)
      end
    end

    context "success" do
      before { Timecop.freeze(Time.zone.parse("2019-01-01")) }
      after { Timecop.return }

      let(:success_response) { File.read("spec/fixtures/enrollment_success_response.json") }

      it "create a billing and bills" do
        student = create(:student)
        create(:admission, student: student)

        billing = Billing.find_by(student: student)
        expect(billing).to be_nil

        post(api_v1_enrollments_path, params: {enrollment: {cpf: student.cpf, desired_due_day: 10}})
        expect(response).to have_http_status(201)
        expect(response.body).to eq(success_response)

        billing = Billing.find_by(student: student)
        expect(billing).not_to be_nil
        expect(billing.desired_due_day).to eq(10)
        expect(billing.status).to eq("openned")
        expect(billing.course_value).to eq(500.00)
        expect(billing.bills.count).to eq(4)

        billing.bills.each do |bill|
          expect(bill.value).to eq(500.00)
          expect(bill.status).to eq("pending")
          expect(bill.payment_method).to eq("bank slip")
        end
      end
    end
  end

  describe "PUT /enrollments/:cpf/renew" do
    context "fails" do
      let(:without_enrollment_key_response) { "{\"message\":\"param is missing or the value is empty: enrollment\"}" }
      let(:student_not_found_response) { "{\"message\":\"Student could not be found\"}" }
      let(:admission_not_found_response) { "{\"message\":\"Admission could not be found\"}" }
      let(:billing_not_found_response) { "{\"message\":\"Billing could not be found\"}" }
      let(:unavailable_to_renew_response) { "{\"message\":\"Renewal not available\"}" }
      let(:billing_not_fully_paid_response) { "{\"message\":\"Billing not fully paid\"}" }

      it "when don't exists student" do
        put(api_v1_enrollment_renew_path(12345678900), params: {})
        expect(response).to have_http_status(404)
        expect(response.body).to eq(student_not_found_response)
      end

      it "when don't exists admission" do
        student = create(:student, cpf: "865.292.211-07")
        put(api_v1_enrollment_renew_path("86529221107"), params: {})
        expect(response).to have_http_status(404)
        expect(response.body).to eq(admission_not_found_response)
      end

      it "when don't exists billing" do
        student = create(:student, cpf: "865.292.211-07")
        create(:admission, student: student)
        put(api_v1_enrollment_renew_path("86529221107"), params: {})
        expect(response).to have_http_status(404)
        expect(response.body).to eq(billing_not_found_response)
      end

      it "when body has no enrollment key" do
        student = create(:student, cpf: "865.292.211-07")
        create(:admission, student: student)
        create(:billing, student: student)
        put(api_v1_enrollment_renew_path("86529221107"), params: {})
        expect(response).to have_http_status(400)
        expect(response.body).to eq(without_enrollment_key_response)
      end

      it "when billing already exists" do
        student = create(:student, cpf: "865.292.211-07")
        create(:admission, student: student)
        billing = build(:billing, student: student)
        billing.initialize_bills
        billing.save!

        put(api_v1_enrollment_renew_path("86529221107"), params: {enrollment: {desired_due_day: 2}})
        expect(response).to have_http_status(422)
        expect(response.body).to eq(unavailable_to_renew_response)
      end

      it "when billing not fully paid" do
        student = create(:student, cpf: "865.292.211-07")
        create(:admission, student: student)
        billing = build(:billing, student: student)
        bills = billing.initialize_bills
        bills.map { |bill| bill.due_date -= 5.months }
        billing.save!

        put(api_v1_enrollment_renew_path("86529221107"), params: {enrollment: {desired_due_day: 2}})
        expect(response).to have_http_status(422)
        expect(response.body).to eq(billing_not_fully_paid_response)
      end
    end

    context "success" do
      before { Timecop.freeze(Time.zone.parse("2019-01-01")) }
      after { Timecop.return }

      let(:success_response) { File.read("spec/fixtures/enrollment_success_renew_response.json") }

      it "when billing not fully paid" do
        student = create(:student, cpf: "865.292.211-07")
        create(:admission, student: student)
        billing = build(:billing, student: student)
        bills = billing.initialize_bills
        bills.map do |bill|
          bill.due_date -= 5.months
          bill.status = "payed"
        end
        billing.save!

        put(api_v1_enrollment_renew_path("86529221107"), params: {enrollment: {desired_due_day: 2}})
        expect(response).to have_http_status(200)
        expect(response.body).to eq(success_response)
      end
    end
  end
end
