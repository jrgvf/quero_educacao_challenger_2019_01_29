require 'rails_helper'

RSpec.describe Student, type: :model do
  context "validates" do
    it "presence of name and cpf" do
      student = Student.new()
      expect(student.valid?).to be_falsey
      expect(student.errors[:name]).to eq(["can't be blank"])
      expect(student.errors[:cpf]).to eq(["is invalid"])
    end

    it "cpf" do
      student = Student.new(name: "Jorge Rodrigues", cpf: "000.000.000-00")
      expect(student.valid?).to be_falsey
      expect(student.errors[:cpf]).to eq(["is invalid"])
    end

    it "uniqueness of cpf" do
      student = create(:student, name: "Jorge Rodrigues")
      new_student = Student.new(name: "Other Name", cpf: student.cpf)
      expect(new_student.valid?).to be_falsey
      expect(new_student.errors[:cpf]).to eq(["has already been taken"])
    end
  end
end
