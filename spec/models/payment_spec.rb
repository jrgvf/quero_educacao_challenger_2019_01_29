require 'rails_helper'

RSpec.describe Payment, type: :model do
  context "validates" do
    it "presence of value" do
      payment = Payment.new()
      expect(payment.valid?).to be_falsey
      expect(payment.errors[:value]).to include("can't be blank")
    end

    it "numericality of value" do
      payment = Payment.new()
      expect(payment.valid?).to be_falsey
      expect(payment.errors[:value]).to include("is not a number")
    end

    it "numericality of value greater_than 0" do
      payment = Payment.new(value: 0)
      expect(payment.valid?).to be_falsey
      expect(payment.errors[:value]).to include("must be greater than 0")
    end
  end

  context "payed?" do
    it "should return false" do
      payment = Payment.new(status: "renewed")
      expect(payment.payed?).to be_falsey
    end

    it "should return true" do
      payment = Payment.new(status: "payed")
      expect(payment.payed?).to be_truthy
    end
  end
end
