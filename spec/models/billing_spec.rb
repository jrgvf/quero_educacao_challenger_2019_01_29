require 'rails_helper'

RSpec.describe Billing, type: :model do
  context "validates" do
    it "presence of student and desired_due_day" do
      billing = Billing.new()
      expect(billing.valid?).to be_falsey
      expect(billing.errors[:student]).to eq(["must exist"])
      expect(billing.errors[:desired_due_day]).to eq(["can't be blank"])
    end
  end

  context "openned?" do
    it "should return false" do
      billing = Billing.new(status: "payed")
      expect(billing.openned?).to be_falsey
    end

    it "should return true" do
      billing = Billing.new(status: "openned")
      expect(billing.openned?).to be_truthy
    end
  end

  context "payed?" do
    it "should return false" do
      billing = Billing.new(status: "renewed")
      expect(billing.payed?).to be_falsey
    end

    it "should return true" do
      billing = Billing.new(status: "payed")
      expect(billing.payed?).to be_truthy
    end
  end

  context "available_renewal?" do
    before { Timecop.freeze(Time.zone.parse("2019-01-01")) }
    after { Timecop.return }

    it "should return false" do
      billing = Billing.new()
      (1..5).each { |n| billing.bills.build(due_date: DateTime.now + n.months) }
      expect(billing.available_renewal?).to be_falsey
    end

    it "should return true" do
      billing = Billing.new(status: "payed")
      (1..5).each { |n| billing.bills.build(due_date: DateTime.now - n.months) }
      expect(billing.available_renewal?).to be_truthy
    end
  end
end
