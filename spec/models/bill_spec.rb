require 'rails_helper'

RSpec.describe Bill, type: :model do
  context "validates" do
    it "presence of year, month and value" do
      bill = Bill.new()
      expect(bill.valid?).to be_falsey
      expect(bill.errors[:year]).to include("can't be blank")
      expect(bill.errors[:month]).to include("can't be blank")
      expect(bill.errors[:value]).to include("can't be blank")
    end

    it "numericality of year, month and value" do
      bill = Bill.new()
      expect(bill.valid?).to be_falsey
      expect(bill.errors[:year]).to include("is not a number")
      expect(bill.errors[:month]).to include("is not a number")
      expect(bill.errors[:value]).to include("is not a number")
    end

    it "numericality of year, month and value greater_than 0" do
      bill = Bill.new(year: 0, month: 0, value: 0)
      expect(bill.valid?).to be_falsey
      expect(bill.errors[:year]).to include("must be greater than 0")
      expect(bill.errors[:month]).to include("must be greater than 0")
      expect(bill.errors[:value]).to include("must be greater than 0")
    end

    it "numericality of month less_than 13" do
      bill = Bill.new(month: 13)
      expect(bill.valid?).to be_falsey
      expect(bill.errors[:month]).to include("must be less than 13")
    end
  end

  context "payed?" do
    it "should return false" do
      bill = Bill.new(status: "renewed")
      expect(bill.payed?).to be_falsey
    end

    it "should return true" do
      bill = Bill.new(status: "payed")
      expect(bill.payed?).to be_truthy
    end
  end

  context "next_bill" do
    before { Timecop.freeze(Time.zone.parse("2019-10-01")) }
    after { Timecop.return }

    it "should return next_bill" do
      billing = create(:billing)
      billing.initialize_bills
      billing.save!

      bill = billing.bills.first
      expect(bill.next_bill).not_to be_nil
    end

    it "should don't return next_bill" do
      billing = create(:billing)
      billing.initialize_bills
      billing.save!

      bill = billing.bills[-2]
      expect(bill.next_bill).not_to be_nil

      bill = billing.bills.last
      expect(bill.next_bill).to be_nil
    end
  end
end
