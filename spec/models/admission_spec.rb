require 'rails_helper'

RSpec.describe Admission, type: :model do
  before { @student = create(:student, name: "Jorge Rodrigues") }

  context "validates" do
    it "presence of student and step" do
      admission = Admission.new(enem_grade: 451)
      expect(admission.valid?).to be_falsey
      expect(admission.errors[:student]).to eq(["must exist"])
      expect(admission.errors[:step]).to eq(["can't be blank"])
    end

    it "numericality of enem_grade" do
      admission = Admission.new(student: @student)
      expect(admission.valid?).to be_falsey
      expect(admission.errors[:enem_grade]).to eq(["is not a number"])
    end

    it "numericality of enem_grade greater_than 450" do
      admission = Admission.new(student: @student, step: 1, enem_grade: 450)
      expect(admission.valid?).to be_falsey
      expect(admission.errors[:enem_grade]).to eq(["must be greater than 450"])
    end
  end
end
