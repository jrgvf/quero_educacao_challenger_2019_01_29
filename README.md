# Quero Educação Challenge Integrated Admission

**Description**

This project was created for resolve a Quero Educação Challenge.

This proposed solution consists in API for manage admission, enrollment and payments.

**Setup**

This project was created using Ruby 2.5.3 and Rails 5.2.1, to install Ruby follow the [instructions](https://rvm.io/rubies/installing) of RVM site (for example).
The project template requires Node.js with Yarn, to install Node follow the [instructions](https://nodejs.org/en/download/package-manager/) and to install Yarn follow the [instructions](https://linuxize.com/post/how-to-install-yarn-on-ubuntu-18-04/).

  * Install dependencies with `bundle install`
  * Install Node.js dependencies with `yarn install`

**Tests**

Run `rspec` on the terminal to run the  tests for this project.
```bash
$ rspec
...............................................................

Finished in 0.77608 seconds (files took 0.98772 seconds to load)
63 examples, 0 failures
```

**Improvements**

 - Implement authentication to ensure service security
 - Run the service on a private network with a Gateway API (Example: Kong)
 - Add logs for monitoring user actions
 - More information about the student and course that will be taken
 - Use a multitenancy architecture, since all classes are linked to the student and so it would be possible to use bank shards
 - Use PostgreSQL as the primary database
 - Use Cache to avoid some queries to the database for data that does not tend to change.
 - Change language and framework for Elixir and Phoenix, because the service will be an API and Elixir with Phoenix performs better than Ruby on Rails.
