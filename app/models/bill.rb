class Bill < ApplicationRecord
  has_many :payments
  belongs_to :billing, counter_cache: :installments_count

  validates :year, presence: true, numericality: { only_integer: true, greater_than: 0}
  validates :month, presence: true, numericality: { only_integer: true, greater_than: 0, less_than: 13 }
  validates :value, presence: true, numericality: { greater_than: 0 }

  scope :by_status, ->(status) { where(status: status) }
  scope :by_year_and_month, ->(year, month) { where(year: year, month: month).limit(1) }

  def to_json_api_format
    {
      value: "R$ #{sprintf('%.2f', self.value)}",
      due_date: self.due_date.to_date,
      paid_date: self.paid_date,
      status: self.status,
      payment_method: self.payment_method,
      month: self.month,
      year: self.year,
      payments: self.payments.map(&:to_json_api_format)
    }
  end

  def payed?
    self.status == "payed"
  end

  def next_bill
    if self.month.next > 12
      self.billing.bills.by_year_and_month(self.year.next, 1).first
    else
      self.billing.bills.by_year_and_month(self.year, self.month.next).first
    end
  end
end
