class Payment < ApplicationRecord
  belongs_to :bill

  before_save :verify_value_payed

  validates :value, presence: true, numericality: { greater_than: 0 }

  scope :by_status, ->(status) { where(status: status) }

  def payed?
    self.status == "payed"
  end

  def to_json_api_format
    {
      value: self.value,
      status: self.status,
      method: self.method,
      expiry_date: self.expiry_date,
      created_at: self.created_at,
      updated_at: self.updated_at
    }
  end

  private

  def verify_value_payed
    if payed? && self.bill.value > self.value.to_f
      difference = (self.bill.value - self.value.to_f).round(2)
      next_bill = self.bill.next_bill
      next_bill.update_attributes!(value: next_bill.value + difference) if next_bill
    end
  end
end
