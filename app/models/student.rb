class Student < ApplicationRecord
  validates_presence_of :name
  validates :cpf, uniqueness: true, cpf: true
end
