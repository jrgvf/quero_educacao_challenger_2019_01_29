class Billing < ApplicationRecord
  BASE_MONTHLY_PAYMENT = 500.00

  belongs_to :student
  has_many :bills
  has_many :payments, through: :bills

  validates :desired_due_day, presence: true

  before_save :try_update_status
  after_destroy :remove_bills, :remove_payments

  def initialize_bills
    now = Time.zone.now
    current_year = now.year

    bill_infos(now).map do |bill_info|
      bill_params = bill_info.merge({value: self.course_value, status: "pending", payment_method: "bank slip"})
      self.bills.build(bill_params)
    end
  end

  def openned?
    self.status == "openned"
  end

  def payed?
    self.status == "payed"
  end

  def available_renewal?
    due_dates = self.bills.map(&:due_date)
    now = Time.zone.now.to_datetime
    due_dates.all? { |due_date| due_date < now }
  end

  def to_json_api_format
    {
      desired_due_day: self.desired_due_day,
      installments_count: self.installments_count,
      status: self.status,
      course_value: "R$ #{sprintf('%.2f', self.course_value)}",
      bills: self.bills.map(&:to_json_api_format)
    }
  end

  private

  def bill_infos(now)
    base_bill_date = now.beginning_of_month + self.desired_due_day.to_i.days - 1.day

    bill_date = base_bill_date + 1.month if now.day > self.desired_due_day.to_i
    bill_date ||= base_bill_date
    bill_date = bill_date.to_datetime.end_of_day
    first_bill_info = {due_date: bill_date, month: base_bill_date.month, year: base_bill_date.year}

    bill_dates = (1..3).inject([first_bill_info]) do |bill_dates, n|
      bill_date = (base_bill_date + n.months).to_datetime.end_of_day
      bill_info = {due_date: bill_date, month: bill_date.month, year: bill_date.year}
      bill_dates << bill_info
    end
  end

  def remove_bills
    self.bills.destroy_all
  end

  def remove_payments
    self.payments.destroy_all
  end

  def try_update_status
    self.status = "payed" if self.bills.all?(&:payed?)
  end
end
