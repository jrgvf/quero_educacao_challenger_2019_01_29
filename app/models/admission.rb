class Admission < ApplicationRecord
  belongs_to :student
  has_one_attached :document

  validates_presence_of :step
  validates :enem_grade, numericality: { only_integer: true, greater_than: 450 }
end
