class Api::V1::EnrollmentsController < ApplicationController
  BASE_MONTHLY_PAYMENT = 500.00

  # POST /api/v1/enrollments
  def create
    @student = Student.find_by(cpf: format_cpf(enrollment_params[:cpf]))
    @admission = Admission.find_by(student: @student) if @student
    @billing = Billing.find_by(student: @student) if @student

    if @student.nil?
      json_response({ message: "Student could not be found" }, :not_found)
    elsif @admission.nil?
      json_response({ message: "Admission could not be found" }, :not_found)
    elsif @billing
      json_response({ message: "Enrollment already completed" }, :unprocessable_entity)
    else
      @billing = Billing.new(create_enrollment_params)
      @billing.initialize_bills()
      @billing.save!
      @admission.update_attributes!(step: "registered")
      json_response(@billing.to_json_api_format, :created)
    end
  end

  # PUT /api/v1/enrollments/:cpf/renew
  def renew
    @student = Student.find_by(cpf: format_cpf(params[:cpf]))
    @admission = Admission.find_by(student: @student) if @student
    @billing = Billing.find_by(student: @student) if @student

    if @student.nil?
      json_response({ message: "Student could not be found" }, :not_found)
    elsif @admission.nil?
      json_response({ message: "Admission could not be found" }, :not_found)
    elsif @billing.nil?
      json_response({ message: "Billing could not be found" }, :not_found)
    elsif !@billing.available_renewal?
      json_response({ message: "Renewal not available" }, :unprocessable_entity)
    elsif @billing.payed?
      @billing.attributes = renew_enrollment_params
      @billing.initialize_bills()
      @billing.save!
      json_response(@billing.to_json_api_format, :ok)
    else
      json_response({ message: "Billing not fully paid" }, :unprocessable_entity)
    end
  end

  private
  # Only allow a trusted enrollment parameters "white list" through.
  def enrollment_params
    params.require(:enrollment).permit(:cpf, :desired_due_day)
  end

  def create_enrollment_params
    enrollment_params.except(:cpf).tap do |_params|
      _params[:student] = @student
      _params[:course_value] = BASE_MONTHLY_PAYMENT
      _params[:status] = "openned"
    end
  end

  def renew_enrollment_params
    enrollment_params.except(:cpf).tap do |_params|
      _params[:course_value] = @billing.course_value * 1.05
      _params[:status] = "renewed"
    end
  end

  def format_cpf(cpf)
    CPF.new(cpf).formatted
  end

end
