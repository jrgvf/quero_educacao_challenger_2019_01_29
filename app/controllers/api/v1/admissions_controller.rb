class Api::V1::AdmissionsController < ApplicationController
  before_action :find_admission, only: [:show]

  # POST /api/v1/admissions
  def create
    @student = Student.find_or_create_by!(student_params)
    @admission = Admission.find_by(student: @student)

    if @admission
      json_response({ message: "Admission already exists" }, :unprocessable_entity)
    else
      Admission.create!(create_admission_params)
      json_response({ message: "Admission approved, please take your enrollment" }, :created)
    end
  end

  # GET /api/v1/admissions/:cpf
  def show
    json_response({
      name: @student.name,
      cpf: @student.cpf,
      enem_grade: @admission.enem_grade,
      step: @admission.step,
      created_at: @admission.created_at,
      updated_at: @admission.updated_at
    }, :ok)
  end

  private
  # Only allow a trusted student parameters "white list" through.
  def student_params
    params.require(:admission).permit(:name, :cpf).tap do |_params|
      _params[:cpf] = format_cpf(_params[:cpf])
    end
  end

  # Only allow a trusted admission parameters "white list" through.
  def create_admission_params
    params.require(:admission).permit(:enem_grade).tap do |_params|
      _params[:student] = @student
      _params[:step] = "admitted"
    end
  end

  def find_admission
    @student = Student.find_by(cpf: format_cpf(params[:cpf]))
    @admission = Admission.find_by(student: @student) if @student
    return json_response({ message: "Admission could not be found" }, :not_found) unless @admission
  end

  def format_cpf(cpf)
    CPF.new(cpf).formatted
  end

end
