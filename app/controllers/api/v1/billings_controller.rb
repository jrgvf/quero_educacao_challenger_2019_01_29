class Api::V1::BillingsController < ApplicationController
  # GET /api/v1/billings/:cpf
  def show
    @student = Student.find_by(cpf: cpf)
    @billing = Billing.find_by(student: @student) if @student

    if @student.nil?
      json_response({ message: "Student could not be found" }, :not_found)
    elsif @billing.nil?
      json_response({ message: "Billing could not be found" }, :not_found)
    else
      json_response(@billing.to_json_api_format, :ok)
    end
  end

  private

  def cpf
    CPF.new(params[:cpf]).formatted
  end

end
