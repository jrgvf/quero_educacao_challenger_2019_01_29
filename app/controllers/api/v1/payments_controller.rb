class Api::V1::PaymentsController < ApplicationController
  # POST /api/v1/billings/:cpf/payments
  def create
    @student = Student.find_by(cpf: cpf)
    @billing = Billing.find_by(student: @student) if @student
    @bill = @billing.bills.by_year_and_month(year, month).first if @billing
    @payment = @bill.payments.first if @bill

    if @student.nil?
      json_response({ message: "Student could not be found" }, :not_found)
    elsif @billing.nil?
      json_response({ message: "Billing could not be found" }, :not_found)
    elsif @bill.nil?
      json_response({ message: "Bill could not be found" }, :not_found)
    elsif @payment
      json_response({ message: "Payment already performed" }, :unprocessable_entity)
    elsif partial_payment? && @bill.next_bill.nil?
      json_response({ message: "There is no open invoice to add the difference" }, :unprocessable_entity)
    else
      @payment = @bill.payments.build(create_payment_params)
      @bill.update_attributes!(status: "payed", paid_date: now)
      @billing.save!
      json_response(@payment.to_json_api_format, :created)
    end
  end

  private

  # Only allow a trusted enrollment parameters "white list" through.
  def payment_params
    params.require(:payment).permit(:month, :year, :value)
  end

  def create_payment_params
    payment_params.except(:month, :year).tap do |_params|
      _params[:status] = "payed"
      _params[:method] = @bill.payment_method
      _params[:expiry_date] = @bill.due_date
    end
  end

  def year
    payment_params[:year]
  end

  def month
    payment_params[:month]
  end

  def cpf
    CPF.new(params[:cpf]).formatted
  end

  def now
    Time.zone.now.to_datetime
  end

  def partial_payment?
    @bill.value > payment_params[:value].to_f
  end

end
