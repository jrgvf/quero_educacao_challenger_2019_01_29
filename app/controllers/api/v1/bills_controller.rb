class Api::V1::BillsController < ApplicationController
  # GET /api/v1/billings/:cpf/bills
  def index
    @student = Student.find_by(cpf: cpf)
    @billing = Billing.find_by(student: @student) if @student

    if @student.nil?
      json_response({ message: "Student could not be found" }, :not_found)
    elsif @billing.nil?
      json_response({ message: "Bills could not be found" }, :not_found)
    else
      bills = @billing.bills.by_status(params[:status]) if params[:status]
      bills ||= @billing.bills
      json_response(bills.map(&:to_json_api_format), :ok)
    end
  end

  private
  
  def cpf
    CPF.new(params[:cpf]).formatted
  end

end
