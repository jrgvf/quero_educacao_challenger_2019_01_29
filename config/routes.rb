Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :students

      resources :admissions, only: [:create, :show], param: :cpf

      resources :enrollments, only: [:create]
      scope "/enrollments/:cpf" do
        put "/renew", controller: :enrollments, action: :renew, as: :enrollment_renew
      end

      resources :billings, only: [:show], param: :cpf
      scope "/billings/:cpf" do
        resources :bills, only: [:index]
        resources :payments, only: [:create]
      end
    end
  end
end
